const std = @import("std");
const c = @cImport(@cInclude("gtk/gtk.h"));

pub fn main() !void {
  const app = c.gtk_application_new("org.llama.filemanager", c.G_APPLICATION_FLAGS_NONE) orelse @panic("null app :(");
  defer c.g_object_unref(app);

  _ = c.g_signal_connect_data(app, "activate",
    @ptrCast(c.GCallback, create_window_on_activate),
    null, null, 0);
  
  _ = c.g_application_run(@ptrCast(*c.GApplication, app), 0, null);
}

fn create_window_on_activate(a: *c.GtkApplication, _: c.gpointer) callconv(.C) void {
  // create a new window, and set its title and size
  const window = c.gtk_application_window_new(a);
  c.gtk_window_set_title(@ptrCast(*c.GtkWindow, window), "File Manager");
  c.gtk_window_set_default_size(@ptrCast(*c.GtkWindow, window), 720, 720);

  // Here we construct the container that is going pack our buttons
  const grid = c.gtk_grid_new();

  // Pack the container in the window
  c.gtk_window_set_child(@ptrCast(*c.GtkWindow, window), grid);

  const button1 = c.gtk_button_new_with_label("Button1");
  //c.g_signal_connect(button, "clicked", @ptrCast(c.GCallback, print_hello), null);
  _ = c.g_signal_connect_data(button1, "clicked", @ptrCast(c.GCallback, print_hello), null, null, std.zig.c_translation.cast(c.GConnectFlags, @as(c_int, 0)));

  // Place the first button in the grid cell (0, 0), and make it fill
  // just 1 cell horizontally and vertically (ie no spanning)
  //
  c.gtk_grid_attach(@ptrCast(*c.GtkGrid, grid), button1, 0, 0, 1, 1);


  const button2 = c.gtk_button_new_with_label("Button2");
  _ = c.g_signal_connect_data(button2, "clicked", @ptrCast(c.GCallback, print_hello), null, null, std.zig.c_translation.cast(c.GConnectFlags, @as(c_int, 0)));

  // Place the second button in the grid cell (1, 0), and make it fill
  // just 1 cell horizontally and vertically (ie no spanning)
  //
  c.gtk_grid_attach(@ptrCast(*c.GtkGrid, grid), button2, 1, 0, 1, 1);
  
  const buttonQ = c.gtk_button_new_with_label("Quit");
  //c.g_signal_connect_swapped(button, "clicked", @ptrCast(c.GCallback, c.gtk_window_destroy), window);
  _ = c.g_signal_connect_data(buttonQ, "clicked", @ptrCast(c.GCallback, c.gtk_window_destroy), window, null, c.G_CONNECT_SWAPPED);

  // Place the Quit button in the grid cell (0, 1), and make it
  // span 2 columns.
  //
  c.gtk_grid_attach (@ptrCast(*c.GtkGrid, grid), buttonQ, 0, 1, 2, 1);

  c.gtk_widget_show(window);
}

fn print_hello (widget: *c.GtkWidget, _: c.gpointer) callconv(.C) void {
  const msg = std.sprintf("Hello World from {s}\n", widget.name);
  c.g_print(msg);
}
