# File Manager
File manager for the llama ecosystem.

## Features
- Very customizable
- Plug-in system
- Compile configuration into the executable including plug-ins for a more perfomant build.
- Uses GTK4

# License
[GPL .V3](https://www.gnu.org/licenses/gpl-3.0.html)

# Build
```
zig build
```
